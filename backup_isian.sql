INSERT INTO `kategoriproduk` (`id`, `namakategori`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Gulali', '', NULL, NULL);

INSERT INTO `produk` (`id`, `id_kategoriproduk`, `kodeproduk`, `namaproduk`, `stok`, `harga`, `diskon`, `foto`, `created_at`, `updated_at`) VALUES
(1, 1, '12345', 'Sample Produk 1', 10, 5000, 0, 'sample1.jpg', NULL, NULL),
(2, 1, '12346', 'Sample Produk 2', 10, 5000, 0, 'sample2.jpg', NULL, NULL),
(5, 1, '123457', 'Sample Produk 3', 10, 5000, 0, 'sample3.jpg', NULL, NULL),
(6, 1, '123458', 'Sample Produk 4', 10, 5000, 0, 'sample4.jpg', NULL, NULL),
(7, 1, '123459', 'Sample Produk 5', 10, 5000, 0, 'sample5.jpg', NULL, NULL),
(8, 1, '123450', 'Sample Produk 6', 10, 5000, 0, 'sample6.jpg', NULL, NULL);

INSERT INTO `users` (`id`, `noktp`, `nama`, `alamat` , `kota` ,`email`, `password`, `remember_token`, `level`, `status`, `fotoktp`, `fotowajah`, `nohp`, `fotoprofile`, `created_at`, `updated_at`) VALUES
(1, '37402346829347', 'Admin GJO', 'Jalan-jalan', 'semarang', 'admin@gjo.com', '$2y$10$qYX1WgLPv9KEN2JS0wCCS.1h25Wu9D6h81S/5X/PB6rxHoc5goXny', NULL, 'admin', '2', '', '', '08123456789', '', NULL, NULL),
(2, '37402346829390', 'GJO Semarang', 'Jalan-jalan', 'semarang', 'gjosmg@gmail.com', '$2y$10$qYX1WgLPv9KEN2JS0wCCS.1h25Wu9D6h81S/5X/PB6rxHoc5goXny', NULL, 'mitra', '2', '', '', '081234723847', '', NULL, NULL);
