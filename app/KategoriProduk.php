<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KategoriProduk extends Model
{
    protected $table = 'kategoriproduk';

    protected $fillable = [
        'namakategori',
        'foto'
    ];

    //Relasi One to Many dari
    public function produk(){
        return $this->hasMany('App\Produk', 'id_kategoriproduk');
    }
}
