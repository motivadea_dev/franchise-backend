<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Perangkat extends Model
{
    protected $table = 'perangkat';

    protected $fillable = [
        'id_users',
        'app_id',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many ke
    public function users(){
        return $this->belongsTo('App\Users', 'id_users');
    }
}
