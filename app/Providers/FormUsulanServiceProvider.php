<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Warga;
use App\Kategori;
use App\Usulan;
use App\Dusun;

class FormUsulanServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('usulan.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarwarga', Warga::lists('nama', 'id'));
            $view->with('daftarkategori', Kategori::lists('namakategori', 'id'));
        });
        
        view()->composer('usulan.index', function($view){
            //Data yang akan di tampilkan
            $view->with('daftardusun', Dusun::lists('nama', 'id'));
            $view->with('daftarkategori', Kategori::lists('namakategori', 'id'));
        });
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
