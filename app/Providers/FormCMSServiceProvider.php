<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\KategoriPost;
use App\Warga;
use App\Posting;

class FormCMSServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('posting.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarkategoripost', KategoriPost::lists('namakategori', 'id'));
            $view->with('daftarwarga', Warga::lists('nama', 'id'));
        });
        view()->composer('userposting.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarkategoripost', KategoriPost::lists('namakategori', 'id'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
