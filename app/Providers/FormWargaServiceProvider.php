<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Warga;
use App\Dusun;
use App\Pendidikan;
use App\Pekerjaan;
use App\Penghasilan;

class FormWargaServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('warga.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftardusun', Dusun::lists('nama', 'id'));
            $view->with('daftarpendidikan', Pendidikan::lists('namapendidikan', 'namapendidikan'));
            $view->with('daftarpekerjaan', Pekerjaan::lists('namapekerjaan', 'namapekerjaan'));
            $view->with('daftarpenghasilan', Penghasilan::lists('namapenghasilan', 'namapenghasilan'));
        });
        view()->composer('keluarga.formanggota', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarwarga', Warga::lists('nama', 'id'));
        });
        view()->composer('user.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftardusun', Dusun::lists('nama', 'id'));
        });
        view()->composer('user.form2', function($view){
            //Data yang akan di tampilkan
            $view->with('daftardusun', Dusun::lists('nama', 'id'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
