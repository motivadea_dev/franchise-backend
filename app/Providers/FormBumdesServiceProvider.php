<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Warga;
use App\KategoriProduk;
use App\SubKategoriProduk;
use App\Toko;
use App\ProfileDesa;


class FormBumdesServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('subkategoriproduk.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarkategoriproduk', KategoriProduk::lists('namakategori', 'id'));
        });
        view()->composer('toko.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarwarga', Warga::lists('nama', 'id'));
        });
        view()->composer('distributor.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftardesa', ProfileDesa::lists('nama', 'id'));
        });
        view()->composer('produktoko.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftartoko', Toko::lists('namatoko', 'id'));
            $view->with('daftarkategoriproduk', KategoriProduk::lists('namakategori', 'id'));
            $view->with('daftarsubkategoriproduk', SubKategoriProduk::lists('namasubkategori', 'id'));
        });
        view()->composer('produkbumdes.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarkategoriproduk', KategoriProduk::lists('namakategori', 'id'));
            $view->with('daftarsubkategoriproduk', SubKategoriProduk::lists('namasubkategori', 'id'));
        });
        view()->composer('transaksipenjualan.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarwarga', Warga::lists('nama', 'id'));
            $view->with('daftartoko', Toko::lists('namatoko', 'id'));
        });
        view()->composer('transaksipenjualanbumdes.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftartoko', Toko::lists('namatoko', 'id'));
        });
        view()->composer('pengiriman.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftartransaksipembelian', TransaksiPembelian::lists('id', 'id'));
            $view->with('daftarkurir', Kurir::lists('nama_kurir', 'id'));
        });
        view()->composer('kurir.form', function($view){
            //Data yang akan di tampilkan
            $view->with('daftarwarga', Warga::lists('nama', 'id'));
            //$view->with('daftarkurir', Kurir::lists('id', 'id'));
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
