<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produk extends Model
{
    protected $table = 'produk';

    protected $fillable = [
        'id_kategoriproduk',
        'kodeproduk',
    	'namaproduk',
        'stok',
        'harga',
        'diskon',
        'foto',
        'created_at',
        'updated_at'
    ];

    //Relasi One to Many ke
    public function kategoriproduk(){
        return $this->belongsTo('App\KategoriProduk', 'id_kategoriproduk');
    }
    
    public function keranjang(){
        return $this->hasMany('App\Keranjang', 'id_produk');
    }
    public function detailpenjualan(){
        return $this->hasMany('App\DetailPenjualan', 'id_produk');
    }
}
