<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransaksiPenjualan extends Model
{
    protected $table = 'transaksipenjualan';

    protected $fillable = [
        'kodepenjualan',
        'id_users',
    	'tanggal',
        'totaldiskon',
        'totalbelanja',
        'subtotal',
        'status',
        'bukti',
        'created_at',
        'updated_at'
    ];

    //Relasi Many to Many ke
    /*public function produktoko(){
        return $this->belongsToMany('App\ProdukToko', 'detailpenjualan', 'id_transaksipenjualan', 'id_produktoko');
    }*/
    public function detailpenjualan(){
        return $this->hasMany('App\DetailPenjualan', 'id_transaksipenjualan');
    }
    //Relasi One to Many ke
    public function users(){
        return $this->belongsTo('App\User', 'id_users');
    }
}
