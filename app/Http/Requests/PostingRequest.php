<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PostingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_kategoripost' => 'required',
            'id_users' => 'required',
            'tanggal' => 'required|date',
            'judul' => 'required',
            'isipost' => 'required',
            //'status' => 'required|in:1,2',
            //'headline' => 'required|in:1,2',
        ];
    }
}
