<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DaftarDesaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'nohp' => 'required',
            'email' => 'required',
            'fotoktp' => 'sometimes|image|max:1024|mimes:jpeg,jpg,bmp,png',   
            'fotowajah' => 'sometimes|image|max:1024|mimes:jpeg,jpg,bmp,png',   
            'username' => 'required|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        ];
    }
}
