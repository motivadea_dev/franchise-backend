<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AntrianRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tanggal' => 'required|date',
            'id_warga' => 'required',
            'id_keperluan' => 'required',
            'status_syarat' => 'required|in:1,2',
            'status_jadi' => 'required|in:1,2',
        ];
    }
}
