<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\TransaksiPenjualan;
use App\DetailPenjualan;
use App\Keranjang;
use App\Produk;
use App\User;
use App\ProdukToko;
use App\Perangkat;
use App\PerangkatToko;

class TransaksiPenjualanController extends Controller
{
    //KONSUMEN
    public function indexapp($item)
    {
        settype($item, "integer");
        $daftartransaksi = TransaksiPenjualan::where('id_users',$item)->orderBy('id', 'desc')->get();
        $koleksi = collect($daftartransaksi);
        $koleksi->toJson();
        return $koleksi;
    }
    public function storeapp(Request $request)  
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        $request->input('tanggal') == date('Y-m-d');
        //2. Simpan Data Transaksi 
        $transaksi = TransaksiPenjualan::create($input);
        //Ambil ID user
        $iduser = $request->input('id_users');
        settype($iduser, "integer");
        //Ambil ID Transaksi
        $id_awal = TransaksiPenjualan::where('id_users', $iduser)->orderBy('id', 'desc')->first();
        $idtransaksi = $id_awal->id;
        settype($idtransaksi, "integer");
        //Tampilkan Keranjang
        $daftar = Keranjang::all();
        $daftarkeranjang = $daftar->where('id_users',$iduser);
       
        //Simpan DetailPenjualan
        foreach($daftarkeranjang as $keranjang){
            $detailpenjualan = New DetailPenjualan;
            $detailpenjualan->id_transaksipenjualan = $idtransaksi;
            $detailpenjualan->id_produk = $keranjang->id_produk;
            $detailpenjualan->jumlah = $keranjang->jumlah;
            $detailpenjualan->save();
            //Update Stok
            // $produk = Produk::findOrFail($keranjang->id_produk);
            // $stokawal = $produk->stok;
            // $stokakhir = $stokawal - $keranjang->jumlah;
            // $produk->stok = $stokakhir;
            // $produk->update();
            //Hapus Keranjang
            $keranjang->delete();
        }
        
        return $transaksi;
    }
    public function detail($item){
        settype($item, "integer"); 
        //Detail Penjualan
        $daftar = DetailPenjualan::with('produk')->where('id_transaksipenjualan',$item)->get();
        //$daftarpenjualan = $daftar->where('id_transaksipenjualan',$item);
        $daftar2 = TransaksiPenjualan::with('users')->where('id',$item)->get();
        //$daftarpenjualan2 = $daftar2->where('id',$item);
        
        $jumlahpenjualan = $daftar2->count();

        //Seleksi
        if($jumlahpenjualan == 0){
            $data = [
            ['id_transaksipenjualan' => null, 'id_produk' => null, 'jumlah' => null, 'produk' => null], 
            ];  
            $data2 = [
            ['id' => null, 'id_users' => null, 'tanggal' => null, 'totaldiskon' => null, 'totalbelanja' => null, 'subtotal' => null, 'status'=> null],
            ]; 
            $koleksi = collect($data);
            $koleksi->toJson();
            $koleksi2 = collect($data2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    public function detail2($item){
        //Detail Penjualan
        $daftar2 = TransaksiPenjualan::with('users')->where('kodepenjualan',$item)->get();
        foreach($daftar2 as $jual){
            $idpenjualan = $jual->id;
            settype($idpenjualan, "integer"); 
        }
        $daftar = DetailPenjualan::with('produk')->where('id_transaksipenjualan',$idpenjualan)->get();
        
        $jumlahpenjualan = $daftar2->count();

        //Seleksi
        if($jumlahpenjualan == 0){
            // $data = [
            // ['id_transaksipenjualan' => null, 'id_produk' => null, 'jumlah' => null, 'produk' => null], 
            // ];  
            $data2 = [
            ['id' => null, 'id_users' => null, 'tanggal' => null, 'totaldiskon' => null, 'totalbelanja' => null, 'subtotal' => null, 'status'=> null],
            ]; 
            // $koleksi = collect($data);
            // $koleksi->toJson();
            $koleksi2 = collect($data2);
            $koleksi2->toJson();
            //return compact('koleksi','koleksi2');
            return $koleksi2;
        }
        else{
            // $koleksi = collect($daftar);
            // $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            //return compact('koleksi','koleksi2');
            return $koleksi2;
        }
    }
    public function detailverifikasi($item){ 
        $daftar2 = TransaksiPenjualan::where('kodepenjualan',$item)->get();
        $idtrans = "";
        foreach($daftar2 as $trans){
            $idtrans = $trans->id;
            settype($idtrans, "integer");
        }
        //Detail Penjualan
        $daftar = DetailPenjualan::with('produk')->where('id_transaksipenjualan',$idtrans)->get();
        $jumlahpenjualan = $daftar2->count();

        //Seleksi
        if($jumlahpenjualan == 0){
            $data = [
            ['id_transaksipenjualan' => null, 'id_produk' => null, 'jumlah' => null, 'produk' => null], 
            ];  
            $data2 = [
            ['id' => null, 'kodepenjualan' => null, 'id_users' => null, 'tanggal' => null, 'totaldiskon' => null, 'totalbelanja' => null, 'subtotal' => null, 'status'=> null, 'bukti' => null],
            ]; 
            $koleksi = collect($data);
            $koleksi->toJson();
            $koleksi2 = collect($data2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    public function updateapp(Request $request)
    {
        $item = $request->id;
        settype($item, "integer"); 
        //1.Pencarian berdasarkan Id 
        $penjualan = TransaksiPenjualan::findOrFail($item);
        $penjualan->status = 4;
        $penjualan->save();
        
        return $penjualan;
    }
    public function verifikasiapp(Request $request)
    {
        $item = $request->id;
        settype($item, "integer"); 
        //1.Pencarian berdasarkan Id 
        $penjualan = TransaksiPenjualan::findOrFail($item);
        $penjualan->bukti = $request->bukti;
        $penjualan->save();
        //Tampilkan Detail Penjualan
        $djual = DetailPenjualan::all();
        $dpenjualan = $djual->where('id_transaksipenjualan',$item);
        foreach($dpenjualan as $detail){
            //Update Stok
            $produk = Produk::findOrFail($detail->id_produk);
            $stokawal = $produk->stok;
            $stokakhir = $stokawal - $detail->jumlah;
            $produk->stok = $stokakhir;
            $produk->update();
        }
        //Seleksi Data Toko
        $idtoko = 1;
    	settype($idtoko, "integer");
    	$daftarperangkat = Perangkat::where('id_users',$idtoko)->get();
    	
    	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        //Jika Usulan diterima
        $content = array("en" => 'Ada order penjualan baru,silahkan diproses.');
        
	    $fields = array(
			'app_id' => "1ad6386b-54f8-42b2-8cbd-d893468b9935",
			'include_player_ids' => $device_id,
			//'include_player_ids' => array("1d4ee16b-3a59-4038-a912-dc868942e1c1"),
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic NDdjNjFjOWYtMGZjZi00NDRjLWE1N2UtMTlmNjEwMDhlZWU5'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
        curl_close($ch);
        
        return $penjualan;
    }
    
    //TOKO
    public function indexappToko()
    {
        $daftartransaksi = TransaksiPenjualan::where('bukti','!=','')->orderBy('id', 'desc')->get();
        $koleksi = collect($daftartransaksi);
        $koleksi->toJson();
        return $koleksi;
    }
    public function indexappSeleksi($awal,$akhir)
    {
        $subtotal = 0;
        $from = date($awal);
        $to = date($akhir);
        $daftartransaksi = TransaksiPenjualan::whereBetween('tanggal', [$from, $to])->where('bukti','!=','')->orderBy('id', 'desc')->get();
        $koleksi = collect($daftartransaksi);
        $koleksi->toJson();
        foreach($daftartransaksi as $transaksi){
            $subtotal = $subtotal    + $transaksi->subtotal;
        }
        $koleksi2 = [
            ['subtotal' => $subtotal],
        ];
        return compact('koleksi','koleksi2');
    }
    public function detailToko($item){
        settype($item, "integer"); 
        //Detail Penjualan
        $daftar = DetailPenjualan::with('produktoko')->where('id_transaksipenjualan',$item)->get();
        //$daftarpenjualan = $daftar->where('id_transaksipenjualan',$item);
        $daftar2 = TransaksiPenjualan::where('id',$item)->get();
        //$daftarpenjualan2 = $daftar2->where('id',$item);
        
        $jumlahpenjualan = $daftar2->count();

        //Seleksi
        if($jumlahpenjualan == 0){
            $data = [
            ['id_transaksipenjualan' => null, 'id_produktoko' => null, 'produktoko' => null],
            ];  
            $data2 = [
            ['id' => null, 'id_warga' => null, 'id_toko' => null, 'tanggal' => null, 'subtotal' => null, 'status'=> null],
            ]; 
            $koleksi = collect($data);
            $koleksi->toJson();
            $koleksi2 = collect($data2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    public function updateappToko(Request $request)
    {
        $item = $request->id;
        settype($item, "integer"); 
        //1.Pencarian berdasarkan Id 
        $penjualan = TransaksiPenjualan::findOrFail($item);
        if($request->status == 'order'){
            $penjualan->status = 2;
        }
        if($request->status == 'proses'){
            $penjualan->status = 3;
        }
        $penjualan->save();
        
        //Seleksi Data User
        $iduser = $request->id_users;
    	settype($iduser, "integer");
    	$daftarperangkat = Perangkat::where('id_users',$iduser)->get();
    	
    	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        //Jika Usulan diterima
        if($request->status == 'order'){
        $content = array("en" => 'Terima kasih konfirmasi pembayarannya,pesanan mu sedang diproses.');
        }
        else if($request->status == 'proses'){
        $content = array("en" => 'Pesanan mu sedang dalam proses pengiriman.');
        }

	    $fields = array(
			'app_id' => "1ad6386b-54f8-42b2-8cbd-d893468b9935",
			'include_player_ids' => $device_id,
			//'include_player_ids' => array("1d4ee16b-3a59-4038-a912-dc868942e1c1"),
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic NDdjNjFjOWYtMGZjZi00NDRjLWE1N2UtMTlmNjEwMDhlZWU5'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
        curl_close($ch);

        return $penjualan;
    }
    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id transaksi
        $transaksi = TransaksiPenjualan::findOrFail($item);
        //2. Hapus data
        $transaksi->delete();
        return $transaksi;
    }
}
