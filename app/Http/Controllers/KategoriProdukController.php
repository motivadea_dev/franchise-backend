<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\KategoriProduk;
use App\SubKategoriProduk;
use App\Produk;
use App\ProdukToko;
use App\ProdukBumdes;

class KategoriProdukController extends Controller
{
    public function indexapp(){
        //1. Menampilkan semua data usulan dan dibagi per halaman 20 baris
        $daftarkategori = KategoriProduk::all();
        //2. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftarkategori);
        $koleksi->toJson();
        return $koleksi;
    }
    public function indexappBumdes(){
        //1. Menampilkan semua data usulan dan dibagi per halaman 20 baris
        $daftarkategori = KategoriProduk::all();
        //2. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftarkategori);
        $koleksi->toJson();
        return $koleksi;
    }
    public function subkategoriapp($item){
        settype($item, "integer"); 
        //Sub Kategori
        $daftar = SubKategoriProduk::where('id_kategoriproduk',$item)->get();
        //$daftarkategori = $daftar->where('id_kategoriproduk',$item);
        $jumlahsub = $daftar->count();
        //Produk Toko
        $daftar2 = Produk::where('id_kategoriproduk',$item)->get();
        //$daftarproduk = $daftar2->where('id_kategoriproduk',$item);

        //Seleksi
        if($jumlahsub == 0){
            $data = [
            ['namasubkategori' => null],
            ];   
            $koleksi = collect($data);
            $koleksi->toJson();

            $data2 = [
            ['namaproduk' => null],
            ];   
            $koleksi2 = collect($data);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    public function subkategoriapp2($idkat,$idsub){
        settype($item, "integer"); 
        //Sub Kategori
        $daftar = SubKategoriProduk::where('id_kategoriproduk',$idkat)->get();
        $jumlahsub = $daftar->count();
        //Produk Toko
        $daftar2 = ProdukToko::where('id_kategoriproduk',$idkat)
        ->where('id_subkategoriproduk',$idsub)->get();

        //Seleksi
        if($jumlahsub == 0){
            $data = [
            ['namasubkategori' => null],
            ];   
            $koleksi = collect($data);
            $koleksi->toJson();

            $data2 = [
            ['namaproduk' => null],
            ];   
            $koleksi2 = collect($data);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
    public function subkategoriappBumdes($item){
        settype($item, "integer"); 
        //Sub Kategori
        $daftar = SubKategoriProduk::where('id_kategoriproduk',$item)->get();
        //$daftarkategori = $daftar->where('id_kategoriproduk',$item);
        $jumlahsub = $daftar->count();
        //Produk Toko
        $daftar2 = ProdukBumdes::where('id_kategoriproduk',$item)->get();
        //$daftarproduk = $daftar2->where('id_kategoriproduk',$item);

        //Seleksi
        if($jumlahsub == 0){
            $data = [
            ['namasubkategori' => null],
            ];   
            $koleksi = collect($data);
            $koleksi->toJson();

            $data2 = [
            ['namaproduk' => null],
            ];   
            $koleksi2 = collect($data);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
        else{
            $koleksi = collect($daftar);
            $koleksi->toJson();
            $koleksi2 = collect($daftar2);
            $koleksi2->toJson();
            return compact('koleksi','koleksi2');
        }
    }
}
