<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\KomentarRequest;
use App\Http\Requests\KomentarPengaduanRequest; 
use App\Http\Requests\DaftarRequest; 
use App\Http\Requests\DaftarDesaRequest;
use App\Http\Requests\PollingRequest; 

use App\Http\Requests;
use App\KategoriPost;
use App\Posting;
use App\Informasi;
use App\KategoriPengaduan;
use App\Pengaduan;
use App\FotoPengaduan;
use App\FotoUsulan;
use App\KomentarPost;
use App\KomentarPengaduan;
use App\BalasKomentar;
use App\BalasKomentarPengaduan;
use App\Usulan;
use App\Polling;
use App\Kategori;
use App\User;
use DB;
use Session;
use Mapper;

class HomePageController extends Controller
{
    //
    public function index(){
		return view('homeshop');
	}
}
