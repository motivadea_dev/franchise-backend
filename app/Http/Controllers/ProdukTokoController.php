<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warga;
use App\Toko;
use App\Produk;
use App\ProdukToko;
use App\ProdukBumdes;
use App\FotoTempProduk;
use App\FotoProduk;
use App\Keranjang;

class ProdukTokoController extends Controller
{

    public function indexapp($item)
    {
        settype($item, "integer");
        $daftar = ProdukToko::where('id_toko',$item)->get();
        $koleksi = collect($daftar);
        $koleksi->toJson();
        return $koleksi;
    }
    public function indexapp2($item)
    {
        $daftar = Produk::all();
        $koleksi = collect($daftar);
        $koleksi->toJson();
        $keranjang = Keranjang::where('id_users',$item)->get();
        $jumlah = $keranjang->count();
        $data = [
            ['jumlahkeranjang' => $jumlah],
            ];  
        $koleksi2 = collect($data);
        $koleksi2->toJson();
        //return $koleksi;
        return compact('koleksi','koleksi2');
    }
    public function indexapp3()
    {
        $daftar = Produk::all();
        $koleksi = collect($daftar);
        $koleksi->toJson();
        return $koleksi;
    }
    public function cariapp($item)
    {
        $daftar = ProdukToko::where('namaproduk', 'LIKE', '%' . $item . '%')->get();
        $koleksi = collect($daftar);
        $koleksi->toJson();
        return $koleksi;
    }
    public function cariappBumdes($item)
    {
        $daftar = ProdukBumdes::where('namaproduk', 'LIKE', '%' . $item . '%')->get();
        $koleksi = collect($daftar);
        $koleksi->toJson();
        return $koleksi;
    }
    //Upload foto ke direktori lokal
    public function uploadFoto(Request $request){
	    $foto = $request->file('file');             //Mengambil Inputan file foto
	    $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)
	
	   if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
	        //$foto_name = "ktp_". date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
	        $foto_name = $request->fotoproduk;
	        $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
	        $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
	        return $foto_name;
	   }
	   return false; 
    }
    public function storeapp(Request $request)
    {
        //1. Mengambil value dari input text
        $input = $request->all();
        //2. Simpan Data usulan 
        $produktoko = Produk::create($input);
        
        return $produktoko;
    }

    public function updateapp(Request $request)
    {
        $item = $request->id;
        settype($item, "integer");
        //1.Pencarian berdasarkan Id produk
        $produktoko = Produk::findOrFail($item);
        //2.Mengambil data dari field edit
        $input = $request->all();
        //3.Menyimpan data produktoko
        $produktoko->update($input);
        return $produktoko;
    }

    public function destroyapp($item)
    {
        //1. Pencarian berdasarkan Id produk
        $produktoko = Produk::findOrFail($item);
        //2. Hapus data
        $produktoko->delete();
        return $produktoko;
    }
}
