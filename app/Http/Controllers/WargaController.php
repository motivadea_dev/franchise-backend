<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Hash;
use App\Http\Requests;
use App\Warga;
use App\Dusun;
use App\Usulan;
use App\Pengaduan;
use App\DataUser;
use App\Perangkat;
use App\User;
use App\Informasi;
use App\KategoriPost;
use App\Posting;
use App\KomentarPost;
use App\BalasKomentar;
use App\Toko;
use App\PerangkatToko; 

class WargaController extends Controller
{
    public function indexawal(){
        //1. Menampilkan semua data Informasi
        $daftar = Informasi::orderBy('id', 'asc')->get();
        //2. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarinformasi = collect($daftar);
        $daftarinformasi->toJson();

        //1. Menampilkan semua data Posting
        $daftar2 = Posting::with('user')->with('kategoripost')->orderBy('id', 'desc')->get();
        //2. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarposting = collect($daftar2);
        $daftarposting->toJson();

        //1. Menampilkan semua data Usulan
        $daftar3 = Usulan::orderBy('id', 'desc')->get();
        //2. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarusulan = collect($daftar3);
        $daftarusulan->toJson();
        
        //1. Menampilkan semua data Pengaduan
        $daftar4 = Pengaduan::orderBy('id', 'desc')->get();
        //2. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarpengaduan = collect($daftar4);
        $daftarpengaduan->toJson();

        return compact('daftarinformasi','daftarposting','daftarusulan','daftarpengaduan');
    }

    public function informasiall(){
        //1. Menampilkan semua data posting dan dibagi per halaman 20 baris
        $daftarposting = Posting::with('user')->with('kategoripost')->orderBy('id', 'desc')->get();
        //2. Menghitung total keseluruhan jumlah posting
        $jumlahposting = Posting::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi = collect($daftarposting);
        $koleksi->toJson();

        //1. Menampilkan semua data posting dan dibagi per halaman 20 baris
        $daftarkategori = KategoriPost::all();
        //2. Menghitung total keseluruhan jumlah posting
        $jumlahkategori = KategoriPost::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $koleksi2 = collect($daftarkategori);
        $koleksi2->toJson();

        return compact('koleksi','koleksi2');
    }
    public function informasi($item){
        settype($item, "integer");
        //1. Menampilkan semua data Informasi dan dibagi per halaman 20 baris
        $daftar = Posting::with('user')->where('id_kategoripost',$item)->orderBy('id', 'desc')->get();
        //2. Menghitung total keseluruhan jumlah Informasi
        $jumlahinformasi = $daftar->count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarinformasi = collect($daftar);
        $daftarinformasi->toJson();
        
        return $daftarinformasi;
    }
    public function informasiuser($item){
        settype($item, "integer");
        //1. Menampilkan semua data Informasi dan dibagi per halaman 20 baris
        $daftar = Posting::with('user')->with('kategoripost')->where('id_users',$item)->orderBy('id', 'desc')->get();
        //2. Menghitung total keseluruhan jumlah Informasi
        $jumlahinformasi = $daftar->count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarinformasi = collect($daftar);
        $daftarinformasi->toJson();
        
        return $daftarinformasi;
    }
    public function detailinformasi($item){
        settype($item, "integer");
        //1. Menampilkan semua data Informasi dan dibagi per halaman 20 baris
        $daftar = Posting::with('user')->where('id',$item)->get();
        //2. Menghitung total keseluruhan jumlah Informasi
        $jumlahinformasi = $daftar->count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarinformasi = collect($daftar);
        $daftarinformasi->toJson();
        
        //1. Menampilkan semua data Informasi dan dibagi per halaman 20 baris
        $daftar2 = KomentarPost::with('balaskomentar')->where('id_posting',$item)->get();
        //2. Menghitung total keseluruhan jumlah Informasi
        $jumlahkomentar = $daftar2->count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarkomentar = collect($daftar2);
        $daftarkomentar->toJson();
        
        return compact('daftarinformasi','jumlahkomentar');
    }
    public function komentarinformasi($item){
        settype($item, "integer");
        //1. Menampilkan semua data Informasi dan dibagi per halaman 20 baris
        $daftar = KomentarPost::with('balaskomentar')->where('id_posting',$item)->get();
        //2. Menghitung total keseluruhan jumlah Informasi
        $jumlahkomentar = KomentarPost::count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        $daftarkomentar = collect($daftar);
        $daftarkomentar->toJson();
        
        return $daftarkomentar;
    }
    public function indexapp($item){
        
        $koleksi = User::with('dusun')->join('warga', function($join) use($item){
      	$join->on('users.username', '=', 'warga.noktp')
                ->where('users.username', '=', $item);
        })->get();
        
        $jumlahwarga = $koleksi->count();
        //Seleksi
        if($jumlahwarga == 0){
            $data = [
            ['noktp' => null],
            ];   
            $kosong = collect($data);
            $kosong->toJson();
            return $kosong;
        }
        else{
            return $koleksi;
        }
    }

    public function cekkosong(){
        $data = [
        ['email' => null, 'password' => null],
        ];   
        $kosong = collect($data);
        $kosong->toJson();
        return $kosong;
    }
    public function loginuser($email,$password)
    {        
        $daftaruser = User::where('users.email', '=', $email)->get();
        $daftaruser->makeVisible('password')->toArray();
        
        //Verifikasi Password
        $jumlahuser = $daftaruser->count();
        if($jumlahuser == 0){
            $data = [
            ['email' => null, 'password' => null],
            ];   
            $kosong = collect($data);
            $kosong->toJson();
            return $kosong;
        }
        else{
            //Cek Password HASH
            foreach($daftaruser as $user){
            $passwordhash = $user->password;
            $status = $user->status;
            }
            if ((Hash::check($password, $passwordhash)) && ($status == '2')) {
		$koleksi = collect($daftaruser);
       		$koleksi->toJson();
        	return $koleksi;
            }
            else{
                $data = [
                ['email' => null, 'password' => null],
                ];   
                $kosong = collect($data);
                $kosong->toJson();
                return $kosong; 
            }
        }
    }

    public function updateapp(Request $request)
    {
    	$idwarga = $request->id;
    	$noktp = $request->noktp;
    	
    	//Warga
        $warga = Warga::findOrFail($idwarga);
        $input = $request->all();
        $warga->update($input);
        
        //User
        if($request->password != ''){
        $input['password'] = bcrypt($input['password']);
        }
        
        return $warga;
    }
    public function updatetoko(Request $request)    // BARU *
    {
    	$idtoko = $request->id;
    	
    	//Toko
        $toko = Toko::findOrFail($idtoko);
        $input = $request->all();
        $toko->update($input);
        
        return $toko;
    }
    
    public function storeapp(Request $request)
    {
        $input = $request->all();
     	//Seleksi Data App
        $appid = $request->input('app_id');
	    $daftarperangkat = Perangkat::where('app_id',$appid)->get();
     	$jumlahperangkat = $daftarperangkat->count();
     	//Cek jika perangkat baru
    	if($jumlahperangkat == 0){
    	$perangkat = Perangkat::create($input);
    	return $perangkat;
    	}
    }
    public function storekomentar(Request $request)
    {
        $input = $request->all();
     	//Seleksi Data App
    	$komentar = KomentarPost::create($input);
    	return $komentar;
    }
    public function storebalasan(Request $request)
    {
        $input = $request->all();
     	//Seleksi Data App
    	$balasan = BalasKomentar::create($input);
    	return $balasan;
    }
    public function cekdaftar($item)
    {
        $daftaruser = User::all();
        $koleksi = $daftaruser->where('email',$item);
        $jumlahuser = $koleksi->count();
        //Seleksi
        if($jumlahuser == 0){
            $data = [
            ['email' => null, 'password' => null],
            ];   
            $kosong = collect($data);
            $kosong->toJson();
            return $kosong;
        }
        else{
            return $koleksi;
        }
    }
    public function daftaruser(Request $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt("gjo123");
        $user = User::create($input);
        
        //Seleksi Data Toko
        $idtoko = 1;
    	settype($idtoko, "integer");
    	$daftarperangkat = Perangkat::where('id_users',$idtoko)->get();
    	
    	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        //Jika Usulan diterima
        $content = array("en" => 'Ada pendaftaran mitra baru,silahkan diproses.');
        
	    $fields = array(
			'app_id' => "1ad6386b-54f8-42b2-8cbd-d893468b9935",
			'include_player_ids' => $device_id,
			//'include_player_ids' => array("1d4ee16b-3a59-4038-a912-dc868942e1c1"),
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic NDdjNjFjOWYtMGZjZi00NDRjLWE1N2UtMTlmNjEwMDhlZWU5'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
        curl_close($ch);

	    return $user;
    }
    public function daftartoko(Request $request)    // BARU *
    {
        $input = $request->all();
	    $toko = Toko::create($input);
	    return $toko;
    }
    public function lupapassword(Request $request) 
    {
        echo "TEST";
    }
    //Upload foto ke direktori lokal
    public function uploadKtp(Request $request){
	    $foto = $request->file('file');             //Mengambil Inputan file foto
	    $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)
	
	   if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
	        //$foto_name = "ktp_". date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
	        $foto_name = $request->fotoktp;
	        $upload_path = 'verifikasi';            //Folder foto sing diupload ke server
	        $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
	        return $foto_name;
	   }
	   return false;   
    }
    
    public function uploadWajah(Request $request){
        $foto = $request->file('file2');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('file2')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            //$foto_name = "selfie_". date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $foto_name = $request->fotowajah;
            $upload_path = 'verifikasi';            //Folder foto sing diupload ke server
            $request->file('file2')->move($upload_path, $foto_name); //Copy paste ke server
	        return $foto_name;
        }
        return false;
    }
    public function uploadBukti(Request $request){
        $foto = $request->file('file3');             //Mengambil Inputan file foto
        $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)

        if($request->file('file3')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
            //$foto_name = "selfie_". date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
            $foto_name = $request->fotobukti;
            $upload_path = 'verifikasi';            //Folder foto sing diupload ke server
            $request->file('file3')->move($upload_path, $foto_name); //Copy paste ke server
	        return $foto_name;
        }
        return false;
    }
    
    public function uploadToko(Request $request){   // BARU * 
	    $foto = $request->file('file');             //Mengambil Inputan file foto
	    $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)
	
	   if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
	        $foto_name = $request->foto;
	        $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
	        $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
	        return $foto_name;
	   }
	   return false;   
    }

    //TOKO
    public function cektoko($item){      
        settype($item, "integer");
        //1. Menampilkan semua data Informasi dan dibagi per halaman 20 baris
        $daftar = Toko::where('id_warga',$item)->get();
        //2. Menghitung total keseluruhan jumlah Informasi
        $jumlahtoko = $daftar->count();
        //3. MEMBUAT DATA API / JSON UNTUK FRONT END MOBILE APPS (IONIC)
        if($jumlahtoko == 0){
            $data = [
            ['kodetoko' => null],
            ];   
            $kosong = collect($data);
            $kosong->toJson();
            return $kosong;
        }
        else{
            $daftaritoko = collect($daftar);
            $daftaritoko->toJson();
        
            return $daftaritoko;
        }
        
    }
    public function perangkattoko(Request $request)
    {
        $input = $request->all();
        //Seleksi Data App
        $appid = $request->input('app_id');
        $daftarperangkat = PerangkatToko::where('app_id',$appid)->get();
        $jumlahperangkat = $daftarperangkat->count();
        //Cek jika perangkat baru
        if($jumlahperangkat == 0){
        $perangkat = PerangkatToko::create($input);
        return $perangkat;
        }
    }
}
