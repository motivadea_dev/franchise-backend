<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\Perangkat;
use Validator;
use Session;
use Auth;

class UserController extends Controller
{
    public function indexapp(){
        $user = User::where('level','=','mitra')->get();
        $jumlahuser = $user->count();
        //Seleksi
        if($jumlahuser > 0){
            $koleksi = collect($user);
            $koleksi->toJson();
        }
        else{
            $data = [
            ['id' => null, 'nama' => null, 'alamat' => null, 'email' => null,],
            ];   
            $koleksi = collect($data);
            $koleksi->toJson();
        }
        return $koleksi;
    }
    public function detail($item){
        settype($item, "integer"); 
        $user = User::where('id',$item)->get();
        $jumlahuser = $user->count();
        //Seleksi
        if($jumlahuser > 0){
            $koleksi = collect($user);
            $koleksi->toJson();
        }
        else{
            $data = [
            ['id' => null, 'nama' => null, 'alamat' => null, 'email' => null,],
            ];   
            $koleksi = collect($data);
            $koleksi->toJson();
        }
        return $koleksi;
    }
    public function updateapp(Request $request)
    {
        $item = $request->id;
        settype($item, "integer"); 
        //1.Pencarian berdasarkan Id 
        $user = User::findOrFail($item);
        if($request->status == 1){
            $user->status = 2;
        }
        if($request->status == 2){
            $user->status = 1;
        }
        $user->save();
        
        //Seleksi Data User
    	$daftarperangkat = Perangkat::where('id_users',$item)->get();
    	
    	//Loop Device ID
        $device_id = array();
        foreach($daftarperangkat as $perangkat){
          $device_id[] = $perangkat->app_id;
        }
        //Jika Usulan diterima
        if($request->status == 1){
        $content = array("en" => 'Selamat,akun anda sudah aktif.Selamat berbisnis');
        }
        else if($request->status == 2){
        $content = array("en" => 'Maaf,Akun kami suspend untuk sementara waktu.');
        }

	    $fields = array(
			'app_id' => "1ad6386b-54f8-42b2-8cbd-d893468b9935",
			'include_player_ids' => $device_id,
			//'include_player_ids' => array("1d4ee16b-3a59-4038-a912-dc868942e1c1"),
      		'data' => array("foo" => "bar"),
			'contents' => $content
		);
		
		$fields = json_encode($fields);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
		'Authorization: Basic NDdjNjFjOWYtMGZjZi00NDRjLWE1N2UtMTlmNjEwMDhlZWU5'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

		$response = curl_exec($ch);
        curl_close($ch);

        return $user;
    }
    public function updateprofile(Request $request)
    {
        $item = $request->id;
        settype($item, "integer"); 
        //1.Pencarian berdasarkan Id 
        $user = User::findOrFail($item);
        $data = $request->all();
        $user->update($data);

        return $user;
    }
        //Upload foto ke direktori lokal
    public function uploadFoto(Request $request){
	    $foto = $request->file('file');             //Mengambil Inputan file foto
	    $ext = $foto->getClientOriginalExtension(); //Extension (.jpg, .png ,dll)
	
	   if($request->file('file')->isValid()){      //Kondisi jika foto valid (nek format e selain jpg/png gak iso)    
	        //$foto_name = "ktp_". date('YmdHis'). ".$ext";   //Penamaan File (berdasarkan tanggal/jam) dadi iso bedo2 orak ketumpuk
	        $foto_name = $request->fotoprofile;
	        $upload_path = 'fotoupload';            //Folder foto sing diupload ke server
            $request->file('file')->move($upload_path, $foto_name); //Copy paste ke server
            
            $item = $request->id;
            settype($item, "integer"); 
            //1.Pencarian berdasarkan Id 
            $user = User::findOrFail($item);
            $user->fotoprofile = $foto_name;
            $user->save();
            
	        return $foto_name;
	   }
	   return false; 
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_list = User::all();
        return view('user.index', compact('user_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $validasi = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:100|unique:users',
            'password' => 'required|confirmed|min:6',
            'password_confirmation' => 'required|min:6',
            'level' => 'required|in:admin,dusun,desa,warga,pelaksana,bumdes',
            'status' => 'required|in:1,2',            
        ]);

        if($validasi->fails()){
            return redirect('user/create')
            ->withInput()
            ->withErrors($validasi);
        }

        //Hash Password
        $data['password'] = bcrypt($data['password']);

        User::create($data);
        Session::flash('flash_message', 'Data pengguna berhasil disimpan');

        return redirect('user');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return view('user.show', compact('user'));
    }
    public function verifikasi(Request $request)
    {
        $user = New User;
        $user = User::findOrFail($request->id);
        $user->status = $request->status;
        $user->update();
        if($request->status == 1){
            Session::flash('flash_message', 'Anda sudah menonaktifkan pengguna.');
        }
        else if($request->status){
            Session::flash('flash_message', 'Aktivasi pengguna berhasil.');
        }
        return redirect('user');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        return view('user.edit', compact('user'));
    }

    public function userprofile()
    {
        $iduser = Auth::id();
        settype($iduser, "integer");
        $user = User::findOrFail($iduser);
        return view('user.edituser', compact('user'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $user = User::findOrFail($id);
        $data = $request->all();

        //Jika ada password
        if($request->has('password')){
            $validasi = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:100|unique:users,username,' . $data['id'],
            'password' => 'required|confirmed|min:6',
            'level' => 'required|in:admin,dusun,desa,warga,pelaksana,bumdes',
            'status' => 'required|in:1,2',   
            ]);
            //Hash Password
            $data['password'] = bcrypt($data['password']);
        }
        //Jika tidak diganti
        else{
            $validasi = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:100|unique:users,username,' . $data['id'],
            'level' => 'required|in:admin,dusun,desa,warga,pelaksana,bumdes',
            'status' => 'required|in:1,2',   
            ]);
            //Hapus password / password tdk diupdate
            $data = array_except($data, ['password']);
        }

        if($validasi->fails()){
            return redirect("user/$id/edit")
            ->withErrors($validasi)
            ->withInput();
        }

        $user->update($data);
        Session::flash('flash_message', 'Data pengguna berhasil diupdate');
        return redirect('user');
    }

    public function update2($id, Request $request)
    {
        $user = User::findOrFail($id);
        $data = $request->all();

        //Jika ada password
        if($request->has('password')){
            $validasi = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:100|unique:users,username,' . $data['id'],
            'password' => 'required|confirmed|min:6', 
            ]);
            //Hash Password
            $data['password'] = bcrypt($data['password']);
        }
        //Jika tidak diganti
        else{
            $validasi = Validator::make($data, [
            'name' => 'required|max:255',
            'username' => 'required|max:100|unique:users,username,' . $data['id'],  
            ]);
            //Hapus password / password tdk diupdate
            $data = array_except($data, ['password']);
        }

        if($validasi->fails()){
            return redirect("userprofile")
            ->withErrors($validasi)
            ->withInput();
        }

        $user->update($data);
        Session::flash('flash_message', 'Data pengguna berhasil diupdate');
        return redirect('userprofile');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
        Session::flash('flash_message', 'Data pengguna berhasil dihapus');
        return redirect('user');
    }
}
