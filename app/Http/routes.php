<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//ROUTE WEBSITE
//=====================================================================
//Home CMS
Route::get('/', 'HomePageController@index');
//HALAMAN ADMIN
//=====================================================================
Route::group(['middleware' => ['web']], function(){
    //Controller Untuk Cek Login
    // Route::auth();
    // Route::get('/', 'HomeController@index');
});

// API MOBILE APPS 
//=====================================================================
//HALAMAN AWAL(OK)
Route::get('api/halamanawal', 'WargaController@indexawal');
// //INDEX
// Route::get('api/toko/{item}', 'WargaController@cektoko');       
// //DAFTAR
// Route::post('api/toko','WargaController@daftartoko');           
// //UPDATE
// Route::put('api/toko','WargaController@updatetoko');           
// //PERANGKAT TOKO
// Route::post('api/perangkattoko','WargaController@perangkattoko'); 
//USULAN
//=====================================================================
//1.USER
//TAMPIL USER
Route::get('api/user', 'UserController@indexapp');
//DETAIL USER
Route::get('api/user/detail/{item}', 'UserController@detail');
//UPDATE VERIFIKASI USER
Route::put('api/user','UserController@updateapp');
//UPDATE PROFILE
Route::put('api/user/profile','UserController@updateprofile');
//INDEX
Route::get('api/warga/{item}', 'WargaController@indexapp');
//LOGIN (OK)
Route::get('api/loginuser','WargaController@cekkosong');
Route::get('api/loginuser/{email}/password/{password}', [
'as' => 'login', 'uses' => 'WargaController@loginuser']);
//UPDATE
Route::put('api/warga','WargaController@updateapp');
//SIMPAN PERANGKAT
Route::post('api/perangkat','WargaController@storeapp');
//DAFTAR USER BARU (OK)
Route::get('api/daftaruser/{item}','WargaController@cekdaftar');
Route::post('api/daftaruser','WargaController@daftaruser');
//UPLOAD (OK)
Route::post('api/uploadktp','WargaController@uploadKtp');
Route::post('api/uploadwajah','WargaController@uploadWajah');
Route::post('api/uploadbukti','WargaController@uploadBukti');
Route::post('api/uploadtoko','WargaController@uploadToko');   
Route::post('api/uploadprofile','UserController@uploadFoto');  
//ECOMMERCE
//=====================================================================
//1. KATEGORI PRODUK 
//INDEX (OK)
Route::get('api/kategoriproduk','KategoriProdukController@indexapp');
//SUB KATEGORI
Route::get('api/kategoriproduk/{item}','KategoriProdukController@subkategoriapp');
//SUB KATEGORI 2
Route::get('api/kategoriproduk/idkat/{idkat}/idsub/{idsub}', [
'as' => 'subkategori', 'uses' => 'KategoriProdukController@subkategoriapp2']);

//2. PRODUK TOKO (OK)
//INDEX
Route::get('api/produktoko/{item}','ProdukTokoController@indexapp');
Route::get('api/produktokohome/{item}','ProdukTokoController@indexapp2');
Route::get('api/produktoko','ProdukTokoController@indexapp3');
//CARI
Route::get('api/produktoko/cari/{item}','ProdukTokoController@cariapp');
//UPLOAD
Route::post('api/uploadproduk','ProdukTokoController@uploadFoto');
//SIMPAN
Route::post('api/produktoko','ProdukTokoController@storeapp');
//UPDATE
Route::put('api/produktoko','ProdukTokoController@updateapp');
//HAPUS
Route::delete('api/produktoko/{item}', 'ProdukTokoController@destroyapp');

//3. KERANJANG (OK)
//INDEX
Route::get('api/keranjang/{item}','KeranjangController@indexapp');
//SIMPAN
Route::post('api/keranjang','KeranjangController@storeapp');
//UPDATE
Route::put('api/keranjang/','KeranjangController@updateapp');
//HAPUS
Route::delete('api/keranjang/{item}', 'KeranjangController@destroyapp');

//4. KERANJANG TOKO
// //INDEX
// Route::get('api/keranjangtoko','KeranjangController@indexappToko');
// //SIMPAN
// Route::post('api/keranjangtoko','KeranjangController@storeappToko');
// //UPDATE
// Route::put('api/keranjangtoko/{item}','KeranjangController@updateappToko');
// //HAPUS
// Route::delete('api/keranjangtoko/{item}', 'KeranjangController@destroyappToko');

//5. PEMBELIAN (MITRA) (OK)
//INDEX
Route::get('api/penjualan/{item}','TransaksiPenjualanController@indexapp');
//DETAIL
Route::get('api/penjualan/detail/{item}','TransaksiPenjualanController@detail');
//DETAIL 2
Route::get('api/penjualan/detail2/{item}','TransaksiPenjualanController@detail2');
//DETAIL Verifikasi
Route::get('api/penjualan/verifikasi/{item}','TransaksiPenjualanController@detailverifikasi');
//SIMPAN
Route::post('api/penjualan','TransaksiPenjualanController@storeapp');
//UPDATE PENERIMAAN BARANG
Route::put('api/penjualan','TransaksiPenjualanController@updateapp');
//VERIFIKASI PEMBAYARAN
Route::put('api/penjualan/verifikasi','TransaksiPenjualanController@verifikasiapp');
//HAPUS
Route::delete('api/penjualan/{item}', 'TransaksiPenjualanController@destroyapp');

//6. PENJUALAN (ADMIN)
//INDEX
Route::get('api/penjualantoko','TransaksiPenjualanController@indexappToko');
//DETAIL
Route::get('api/penjualantoko/detail/{item}','TransaksiPenjualanController@detailToko');
//UPDATE
Route::put('api/penjualantoko','TransaksiPenjualanController@updateappToko');
//SELEKSI TANGGAL
Route::get('api/penjualantoko/awal/{awal}/akhir/{akhir}', [
    'as' => 'penjualantoko', 'uses' => 'TransaksiPenjualanController@indexappSeleksi']);


