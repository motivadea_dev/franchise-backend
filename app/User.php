<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'noktp',
        'nama', 
        'alamat',
        'kota',
        'email',
        'password',
        'level',
        'status',
        'fotoktp',
        'fotowajah',
        'nohp',
        'fotoprofile',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function keranjang(){
        return $this->hasMany('App\Keranjang', 'id_users');
    }
    public function transaksipenjualan(){
        return $this->hasMany('App\TransaksiPenjualan', 'id_users');
    }
    public function perangkat(){
        return $this->hasMany('App\Perangkat', 'id_users');
    }
}
