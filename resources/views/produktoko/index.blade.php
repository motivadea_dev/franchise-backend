@extends('template')

@section('main')
<div id="produktoko" class="panel panel-default">
	<div class="panel-heading"><b><h4>Produk Toko</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	<div class="tombol-nav">
		{{ link_to('produktoko/create','Tambah Produk Toko',['class' => 'btn btn-primary']) }}
	</div><br><br><br>
	@if (count($daftarproduktoko) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>Kode Produk</th>
				<th>Nama Produk</th>
				<th>Stok</th>
				<th>Harga</th>
				<th>Diskon</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarproduktoko as $produktoko): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $produktoko->kodeproduk }}</td>
				<td>{{ $produktoko->namaproduk }}</td>
				<td>{{ $produktoko->stok }}</td>
				<td>{{ $produktoko->harga }}</td>
				<td>{{ $produktoko->diskon }}</td>

				<!-- Tombol -->
				<td>
					<div class="box-button">
					{{ link_to('produktoko/' . $produktoko->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div> 
					<div class="box-button">
					{{ link_to('produktoko/' . $produktoko->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['ProduktokowebController@destroy',$produktoko->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('produktoko/print/' . $produktoko->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak Ada Nama Produk Toko</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Toko : {{ $jumlahproduktoko }}</strong>
	</div>
	<div class="paging">
	{{ $daftarproduktoko->links() }}
	</div>
	</div>

	</div>
</div>
@stop

@section('footer')
	@include('footer')
@stop