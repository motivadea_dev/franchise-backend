@extends('template')
@section('main')
<div id="produktoko" class="panel panel-default">
	<div class="panel-heading"><b><h4>Nama Produk Toko</h4></b></div>
	<div class="panel-body">
		<table class="table table-striped">
		<tr><th>Kategori Produk</th><td>{{ $produktoko->kategoriproduk->namakategori }}
		</td></tr>
		<tr><th>Sub Kategori Produk</th><td>{{ $produktoko->subkategoriproduk->namasubkategori }}
		</td></tr>
		<tr><th>Nama Toko</th><td>{{ $produktoko->toko->namatoko }}
		</td></tr>
		<tr><th>Kode Produk</th><td>{{ $produktoko->kodeproduk }}
		</td></tr>
		<tr><th>Nama Produk</th><td>{{ $produktoko->namaproduk }}</td></tr>
		<tr><th>Stok Barang</th><td>{{ $produktoko->stok }}
		</td></tr>
		<tr><th>Harga</th><td>{{ $produktoko->harga }}
		</td></tr>
		<tr><th>Diskon</th><td>{{ $produktoko->diskon }}
		</td></tr>
		<tr><th>Foto</th><td><img width="200" height="200" src="{{ asset('fotoupload/' . $produktoko->foto) }}">
		</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop