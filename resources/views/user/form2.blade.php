@if (isset($user))
{!! Form::hidden('id', $user->id) !!}
@endif

{{-- Nama --}}
@if($errors->any())
<div class="form-group {{ $errors->has('name') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('name','Nama',['class' => 'control-label']) !!}
	{!! Form::text('name', null,['class' => 'form-control']) !!}
	@if ($errors->has('name'))
	<span class="help-block">{{ $errors->first('name') }}</span>
	@endif
</div>

{{-- Username --}}
@if($errors->any())
<div class="form-group {{ $errors->has('username') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('username','Username',['class' => 'control-label']) !!}
	{!! Form::text('username', null,['class' => 'form-control']) !!}
	@if ($errors->has('username'))
	<span class="help-block">{{ $errors->first('username') }}</span>
	@endif
</div>

{{-- Status --}}
{!! Form::hidden('status', 1) !!}

{{--  Dusun --}}
<div class="form-group">
	{!! Form::label('id_dusun','Dusun',['class' => 'control-label']) !!}
	@if(count($daftardusun) > 0)
	{!! Form::select('id_dusun', $daftardusun, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_dusun','placeholder'=>'Pilih Dusun']) !!}
	@else
	<p>Tidak ada pilihan dusun,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_dusun'))
	<span class="help-block">{{ $errors->first('id_dusun') }}</span>
	@endif
</div>

{{-- Password --}}
@if($errors->any())
<div class="form-group {{ $errors->has('password') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('password','Password',['class' => 'control-label']) !!}
	{!! Form::password('password', ['class' => 'form-control']) !!}
	@if ($errors->has('password'))
	<span class="help-block">{{ $errors->first('password') }}</span>
	@endif
</div>

{{-- Password Confirm --}}
@if($errors->any())
<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('password_confirmation','Konfirmasi Password',['class' => 'control-label']) !!}
	{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
	@if ($errors->has('password_confirmation'))
	<span class="help-block">{{ $errors->first('password_confirmation') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>

<script>
$(document).ready(function(){
	$('.js-example-basic-single').select2();
});
</script>