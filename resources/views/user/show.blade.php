@extends('template')
@section('main')
<div id="user" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Pengguna</h4></b></div>
	<div class="panel-body">
@if (isset($user))
	{!! Form::model($user, ['method' => 'PATCH', 'action' => ['UserController@verifikasi', $user->id],'files'=>true]) !!}
	{!! Form::hidden('id', $user->id) !!}
	
	{{-- Status --}}
	@if($errors->any())
	<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
	@else
	<div class="form-group">
	@endif
	{!! Form::label('status','Akses Login',['class' => 'control-label']) !!}
		<div class="radio">
		<label>{!! Form::radio('status','1') !!} Belum Aktif
		</label>
		</div>
		<div class="radio">
		<label>{!! Form::radio('status','2') !!} Aktif
		</label>
		</div>
		@if ($errors->has('status'))
		<span class="help-block">{{ $errors->first('status') }}</span>
		@endif
	</div>
	{{-- Tombol --}}
	<div class="row">
		<div class="col-md-3">
		<div class="form-group">
		{!! Form::submit('Konfirm Aktivasi User',['class' => 'btn btn-success form-control']) !!}
		</div>
		</div>
	</div>
	{!! Form::close() !!}
@endif
		<table class="table table-striped">
		<tr><th>Nama</th><td>{{ $user->name }}</td></tr>
		<tr><th>No.KTP</th><td>{{ $user->username }}</td></tr>
		<tr><th>Level</th><td>{{ $user->level }}</td></tr>
		<tr><th>Status Login</th><td>
		@if( $user->status == 1 )
		Belum Aktif
		@elseif( $user->status == 2 )
		Aktif
		@endif
		</td></tr>
		@if($user->fotoktp != '')
		<tr><th>Foto KTP</th><td><img width="300" height="300" src="{{ asset('verifikasi/' . $user->fotoktp) }}"></td></tr>
		@else
		<tr><th>Foto KTP</th><td><img width="300" height="300" src="{{ asset('fotoupload/nophoto.jpg') }}"></td></tr>
		@endif
		@if($user->fotowajah!= '')
		<tr><th>Foto Wajah + KTP</th><td><img width="300" height="300" src="{{ asset('verifikasi/' . $user->fotowajah) }}"></td></tr>
		@else
		<tr><th>Foto Wajah + KTP</th><td><img width="300" height="300" src="{{ asset('fotoupload/nophoto.jpg') }}"></td></tr>
		@endif
		<tr><th>Dusun</th><td>{{ $user->dusun->nama }}</td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop