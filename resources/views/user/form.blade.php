@if (isset($user))
{!! Form::hidden('id', $user->id) !!}
@endif

{{-- Nama --}}
@if($errors->any())
<div class="form-group {{ $errors->has('name') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('name','Nama',['class' => 'control-label']) !!}
	{!! Form::text('name', null,['class' => 'form-control']) !!}
	@if ($errors->has('name'))
	<span class="help-block">{{ $errors->first('name') }}</span>
	@endif
</div>

{{-- Username --}}
@if($errors->any())
<div class="form-group {{ $errors->has('username') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('username','Username',['class' => 'control-label']) !!}
	{!! Form::text('username', null,['class' => 'form-control']) !!}
	@if ($errors->has('username'))
	<span class="help-block">{{ $errors->first('username') }}</span>
	@endif
</div>

{{-- Level --}}
@if($errors->any())
<div class="form-group {{ $errors->has('level') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('level','Level',['class' => 'control-label']) !!}
	<div class="radio">
	<label>{!! Form::radio('level','admin') !!} Super Administrator
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('level','desa') !!} Admin Desa
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('level','dusun') !!} Admin Dusun
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('level','warga') !!} Warga Desa
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('level','pelaksana') !!} Pelaksana Kegiatan/Lapangan
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('level','bumdes') !!} Admin Bumdes
	</label>
	</div>
	@if ($errors->has('level'))
	<span class="help-block">{{ $errors->first('level') }}</span>
	@endif
</div>

{{-- Status --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Akses Login',['class' => 'control-label']) !!}
	<div class="radio">
	<label>{!! Form::radio('status','1') !!} Belum Aktif
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','2') !!} Aktif
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{--  Dusun --}}
<div class="form-group">
	{!! Form::label('id_dusun','Dusun',['class' => 'control-label']) !!}
	@if(count($daftardusun) > 0)
	{!! Form::select('id_dusun', $daftardusun, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_dusun','placeholder'=>'Pilih Dusun']) !!}
	@else
	<p>Tidak ada pilihan dusun,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_dusun'))
	<span class="help-block">{{ $errors->first('id_dusun') }}</span>
	@endif
</div>

{{-- Password --}}
@if($errors->any())
<div class="form-group {{ $errors->has('password') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('password','Password',['class' => 'control-label']) !!}
	{!! Form::password('password', ['class' => 'form-control']) !!}
	@if ($errors->has('password'))
	<span class="help-block">{{ $errors->first('password') }}</span>
	@endif
</div>

{{-- Password Confirm --}}
@if($errors->any())
<div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('password_confirmation','Konfirmasi Password',['class' => 'control-label']) !!}
	{!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
	@if ($errors->has('password_confirmation'))
	<span class="help-block">{{ $errors->first('password_confirmation') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>

<script>
$(document).ready(function(){
	$('.js-example-basic-single').select2();
});
</script>