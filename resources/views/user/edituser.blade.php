@extends('template')

@section('main')
	<div id="user" class="panel panel-default">
		<div class="panel-heading"><b><h4>Edit Pengguna</h4></b></div>
		<div class="panel-body">
		@include('_partial.flash_message')
		{!! Form::model($user, ['method' => 'PATCH', 'action' => ['UserController@update2', $user->id]]) !!}
		@include('user.form2', ['submitButtonText' => 'Simpan'])
		{!! Form::close() !!}
		</div>
	</div>
@stop

@section('footer')
	@include('footer')
@stop