@if (isset($transaksipenjualan))
{!! Form::hidden('id', $transaksipenjualan->id) !!}
@endif

{{-- Tanggal --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggal','Tanggal',['class' => 'control-label']) !!}
	{!! Form::date('tanggal', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggal'))
	<span class="help-block">{{ $errors->first('tanggal') }}</span>
	@endif
</div>

{{--  Toko --}}
<div class="form-group">
	{!! Form::label('id_toko','Toko',['class' => 'control-label']) !!}
	@if(count($daftartoko) > 0)
	{!! Form::select('id_toko', $daftartoko, null,['class' => 'form-control', 'id'=>'id_toko','placeholder'=>'Pilih Toko']) !!}
	@else
	<p>Tidak ada pilihan Toko,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_toko'))
	<span class="help-block">{{ $errors->first('id_toko') }}</span>
	@endif
</div>

{{--  Warga --}}
<div class="form-group">
	{!! Form::label('id_warga','Warga',['class' => 'control-label']) !!}
	@if(count($daftarwarga) > 0)
	{!! Form::select('id_warga', $daftarwarga, null,['class' => 'form-control', 'id'=>'id_warga','placeholder'=>'Pilih Warga']) !!}
	@else
	<p>Tidak ada pilihan Warga,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_warga'))
	<span class="help-block">{{ $errors->first('id_warga') }}</span>
	@endif
</div>

{{-- Total Diskon --}}
@if($errors->any())
<div class="form-group {{ $errors->has('totaldiskon') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('totaldiskon','Total Diskon',['class' => 'control-label']) !!}
	{!! Form::text('totaldiskon', null,['class' => 'form-control']) !!}
	@if ($errors->has('totaldiskon'))
	<span class="help-block">{{ $errors->first('totaldiskon') }}</span>
	@endif
</div>

{{-- Total Belanja --}}
@if($errors->any())
<div class="form-group {{ $errors->has('totalbelanja') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('totalbelanja','Total Belanja',['class' => 'control-label']) !!}
	{!! Form::text('totalbelanja', null,['class' => 'form-control']) !!}
	@if ($errors->has('totalbelanja'))
	<span class="help-block">{{ $errors->first('totalbelanja') }}</span>
	@endif
</div>

{{-- Sub Total Belanja --}}
@if($errors->any())
<div class="form-group {{ $errors->has('subtotal') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('subtotal','Sub Total',['class' => 'control-label']) !!}
	{!! Form::text('subtotal', null,['class' => 'form-control']) !!}
	@if ($errors->has('subtotal'))
	<span class="help-block">{{ $errors->first('subtotal') }}</span>
	@endif
</div>

{{-- Status --}}
@if($errors->any())
<div class="form-group {{ $errors->has('status') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('status','Status',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('status','order') !!} Order
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','kirim') !!} Kirim
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('status','selesai') !!} Selesai
	</label>
	</div>
	@if ($errors->has('status'))
	<span class="help-block">{{ $errors->first('status') }}</span>
	@endif
</div>

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>