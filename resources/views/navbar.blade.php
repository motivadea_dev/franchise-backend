<nav class="navbar navbar-default">
<div class="container-fluid">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle collapsed" 
				data-toggle="collapse"
				data-target="#bs-example-navbar-collapse-1"
				aria-expanded="false">
			<span class="sr-only">Toggle Navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
			<a class="navbar-brand" href="{{ url('/')}}"><b>SEDESA.id</b></a>
	</div>
	<div class="collapse navbar-collapse"
		 id="bs-example-navbar-collapse-1">
	@if (Auth::check())
		@if((Auth::user()->level == 'admin') || (Auth::user()->level == 'desa'))
		 <ul class="nav navbar-nav">
		 	<li class="dropdown">
		 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Data Master <span class="caret"></span></a>
		 		<ul class="dropdown-menu" role="menu">
				 	<li><a href="{{ url('user') }}">Data Pengguna (Login)</a></li>
					<li><a href="{{ url('dusun') }}">Data Dusun</a></li>
					<li><a href="{{ url('pendidikan') }}">Pendidikan </a></li>
					<li><a href="{{ url('pekerjaan') }}">Pekerjaan </a></li>
					<li><a href="{{ url('penghasilan') }}">Penghasilan </a></li>
                </ul>
		 	</li>
		 </ul>
		 <ul class="nav navbar-nav">
		 	<li class="dropdown">
		 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Kependudukan <span class="caret"></span></a>
		 		<ul class="dropdown-menu" role="menu">
				 	<li><a href="{{ url('warga') }}">Data Warga</a></li>
				 	<li><a href="{{ url('keluarga') }}">Data Keluarga</a></li>
				 	<li><a href="{{ url('dasawisma') }}">Dasa Wisma <span class="label label-danger"><i>Progress</i></span></a></li>
                </ul>
		 	</li>
		 </ul>
		 <ul class="nav navbar-nav">
		 	<li class="dropdown">
		 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Musyawarah Desa <span class="caret"></span></a>
		 		<ul class="dropdown-menu" role="menu">
				 	<li><a href="{{ url('kategori') }}">Kategori Usulan</a></li>
				 	<li><a href="{{ url('usulan') }}">Usulan Warga</a></li>
					<li><a href="{{ url('hasil') }}">Hasil Musyawarah</a></li>
                </ul>
		 	</li>
		 </ul>
		 <ul class="nav navbar-nav">
		 	<li class="dropdown">
		 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> E-commerce Bumdes <span class="caret"></span></a>
		 		<ul class="dropdown-menu" role="menu">
					<li><a href="{{ url('kategoriproduk') }}"> Kategori Produk</a></li>
					<li><a href="{{ url('subkategoriproduk') }}">Sub Kategori Produk</a></li>
					<li><a href="{{ url('distributor') }}">Daftar Distributor</a></li>
					<li><a href="{{ url('produkbumdes') }}">Produk Distributor</a></li>
					<li><a href="{{ url('transaksipenjualanbumdes') }}"> Pemesanan Distributor</a></li>
					<li><a href="{{ url('toko') }}">Daftar Toko</a></li>
					<li><a href="{{ url('produktoko') }}">Produk Toko</a></li>
					<li><a href="{{ url('transaksipenjualan') }}"> Penjualan Toko</a></li>
					<li><a href="{{ url('kurir') }}"> Kurir</a></li>
					<li><a href="{{ url('pengiriman') }}"> Pengiriman</a></li>
					<li><a href="{{ url('pesan') }}"> Pesan <span class="label label-danger"><i>Progress</i></span></a></li>
					<li><a href="{{ url('dompet') }}"> Dompet <span class="label label-danger"><i>Progress</i></span></a></li>
                </ul>
		 	</li>
		 </ul>
		 <ul class="nav navbar-nav">
		 	<li class="dropdown">
		 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> Pelayanan Warga <span class="caret"></span></a>
		 		<ul class="dropdown-menu" role="menu">
				 	<li><a href="{{ url('keperluan') }}">Kategori Layanan</a></li>
					 <li><a href="{{ url('antrian') }}">Layanan (Antrian)</a></li>
					 <li><a href="{{ url('kategoripengaduan') }}">Kategori Pengaduan</a></li>
				 	<li><a href="{{ url('pengaduan') }}">Pengaduan Warga</a></li>
                </ul>
		 	</li>
		 </ul>
		<ul class="nav navbar-nav">
		 	<li><a href="{{ url('pemetaan') }}">Pemetaan Wilayah</a></li>
		 </ul>
		 <ul class="nav navbar-nav">
		 	<li class="dropdown">
		 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> CMS <span class="caret"></span></a>
		 		<ul class="dropdown-menu" role="menu">
				 	 <li><a href="{{ url('kategoripost') }}">Kategori Posting</a></li>
					 <li><a href="{{ url('posting') }}">Posting</a></li>
					 <li><a href="{{ url('komentarpost') }}">Semua Komentar</a></li>
					 <li><a href="{{ url('polling') }}">Semua Polling</a></li>
                </ul>
		 	</li>
		 </ul>
		 <ul class="nav navbar-nav navbar-right">
		 	<li class="dropdown">
		 		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"> {{ Auth::user()->name }} <span class="caret"></span></a>
		 		<ul class="dropdown-menu" role="menu">
				 	<li><a href="{{ url('profiledesa') }}"><i class="glyphicon glyphicon-home"></i> Profile Desa</a></li>
                    <li><a href="{{ url('logout') }}"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
                </ul>
		 	</li>
		 </ul>
		@elseif(Auth::user()->level == 'warga')
		 <ul class="nav navbar-nav">
		 	<li><a href="{{ url('userposting') }}"><i class="glyphicon glyphicon-pencil"></i> Semua Posting</a></li>
			<li><a href="{{ url('userposting/create') }}"><i class="glyphicon glyphicon-plus"></i> Tambah Posting</a></li>
			<li><a href="{{ url('userprofile') }}"><i class="glyphicon glyphicon-user"></i> Profile</a></li>
			<li><a href="{{ url('home') }}" target="_blank"><i class="glyphicon glyphicon-globe"></i> Halaman Depan</a></li>
			<li><a href="{{ url('logout') }}"><i class="glyphicon glyphicon-off"></i> Logout</a></li>
		 </ul>
		@endif
	@endif
	</div>
</div>
</nav>

