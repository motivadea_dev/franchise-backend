@extends('template')
@section('main')
<div id="produk" class="panel panel-default">
	<div class="panel-heading"><b><h4>Data Warga</h4></b></div>
	<div class="panel-body">
		<table class="table table-striped">
		<tr><th>No KTP</th><td>{{ $warga->noktp }}
		</td></tr>
		<tr><th>Nama Desa</th><td>{{ $warga->nama }}</td></tr>
		<tr><th>Alamat</th><td>{{ $warga->alamat }}</td></tr>
		<tr><th>Tanggal Lahir</th><td>{{ $warga->tanggal_lahir }}</td></tr>
		<tr><th>Jenis Kelamin</th><td>
		@if( $warga->jenis_kelamin == 'L' )
		Laki-laki
		@elseif( $warga->jenis_kelamin == 'P' )
		Perempuan
		@endif
		</td></tr>
		<tr><th>Pendidikan</th><td>{{ $warga->namapendidikan }}</td></tr>
		<tr><th>Pekerjaan</th><td>{{ $warga->namapekerjaan }}</td></tr>
		<tr><th>Penghasilan</th><td>{{ $warga->namapenghasilan }}</td></tr>
		<tr><th>Foto</th><td><img width="200" height="200" src="{{ asset('fotoupload/' . $warga->foto) }}">
		</td></tr>
		<tr><th>Dusun</th><td>{{ $warga->dusun->nama }}</td></tr>
		<tr><th>No.Telp</th><td>{{ !empty($warga->datauser->nomor_telepon) ? $warga->datauser->nomor_telepon : '-' }}</td></tr>
		<tr><th>Email</th><td>{{ !empty($warga->datauser->email) ? $warga->datauser->email : '-' }}<td></tr>
		</table>
	</div>
	</div>
</div>
@stop

@section('footer')
@include('footer')
@stop