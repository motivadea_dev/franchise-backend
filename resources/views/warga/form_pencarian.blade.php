<div id="pencarian">
	{!! Form::open(['url' => 'warga/cari', 'method' => 'GET', 'id' => 'form_pencarian']) !!}
<div class="row">
	<div class="col-md-8">
		<div class="input-group">
			{!! Form::text('kata_kunci',(!empty($kata_kunci)) ? $kata_kunci : null,['class'=>'form-control','placeholder'=> 'Masukkan NO KTP']) !!}
			<span class="input-group-btn">
				{!! Form::button('<i class="glyphicon glyphicon-search"></i>', ['class'=>'btn btn-default','type'=>'submit']) !!}
			</span>
		</div>
	</div>
</div>
	{!! Form::close() !!}
</div>