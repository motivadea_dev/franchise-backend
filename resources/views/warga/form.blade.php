@if (isset($warga))
{!! Form::hidden('id', $warga->id) !!}
@endif

{{-- NOKTP --}}
@if($errors->any())
<div class="form-group {{ $errors->has('noktp') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('noktp','No KTP',['class' => 'control-label']) !!} 
	{!! Form::text('noktp', null,['class' => 'form-control']) !!}
	@if ($errors->has('noktp'))
	<span class="help-block">{{ $errors->first('noktp') }}</span>
	@endif
</div>

{{-- Nama --}}
@if($errors->any())
<div class="form-group {{ $errors->has('nama') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('nama','Nama Warga',['class' => 'control-label']) !!}
	{!! Form::text('nama', null,['class' => 'form-control']) !!}
	@if ($errors->has('nama'))
	<span class="help-block">{{ $errors->first('nama') }}</span>
	@endif
</div>

{{-- Alamat --}}
@if($errors->any())
<div class="form-group {{ $errors->has('alamat') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('alamat','Alamat Warga',['class' => 'control-label']) !!}
	{!! Form::text('alamat', null,['class' => 'form-control']) !!}
	@if ($errors->has('alamat'))
	<span class="help-block">{{ $errors->first('alamat') }}</span>
	@endif
</div>

{{--  Dusun --}}
<div class="form-group">
	{!! Form::label('id_dusun','Dusun',['class' => 'control-label']) !!}
	@if(count($daftardusun) > 0)
	{!! Form::select('id_dusun', $daftardusun, null,['class' => 'form-control js-example-basic-single', 'id'=>'id_dusun','placeholder'=>'Pilih Dusun']) !!}
	@else
	<p>Tidak ada pilihan dusun,silahkan buat dulu.</p>
	@endif
	@if ($errors->has('id_dusun'))
	<span class="help-block">{{ $errors->first('id_dusun') }}</span>
	@endif
</div>

{!! Form::hidden('id_profiledesa', '1') !!}

{{-- Tanggal Lahir --}}
@if($errors->any())
<div class="form-group {{ $errors->has('tanggal_lahir') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('tanggal_lahir','Tanggal Lahir',['class' => 'control-label']) !!}
	{!! Form::date('tanggal_lahir', null,['class' => 'form-control']) !!}
	@if ($errors->has('tanggal_lahir'))
	<span class="help-block">{{ $errors->first('tanggal_lahir') }}</span>
	@endif
</div>

{{-- Jenis Kelamin --}}
@if($errors->any())
<div class="form-group {{ $errors->has('jenis_kelamin') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
{!! Form::label('jenis_kelamin','Jenis Kelamin',['class' => 'control-label']) !!}
	<div class="radio">
	<label>
	{!! Form::radio('jenis_kelamin','L') !!} Laki-laki
	</label>
	</div>
	<div class="radio">
	<label>{!! Form::radio('jenis_kelamin','P') !!} Perempuan
	</label>
	</div>
	@if ($errors->has('jenis_kelamin'))
	<span class="help-block">{{ $errors->first('jenis_kelamin') }}</span>
	@endif
</div>

{{-- Nama Pendidikan --}}
<div class="form-group">
{!! Form::label('namapendidikan','Pendidikan',['class' => 'control-label']) !!}
@if(count($daftarpendidikan) > 0)
{!! Form::select('namapendidikan', $daftarpendidikan, null,['class' => 'form-control js-example-basic-single', 'id'=>'namapendidikan','placeholder'=>'Pilih Pendidikan']) !!}
@else
<p>Tidak ada pilihan Pendidikan,silahkan buat dulu.</p>
@endif
@if ($errors->has('namapendidikan'))
<span class="help-block">{{ $errors->first('namapendidikan') }}</span>
@endif
</div>

{{-- Nama Pekerjaan --}}
<div class="form-group">
{!! Form::label('namapekerjaan','Pekerjaan',['class' => 'control-label']) !!}
@if(count($daftarpekerjaan) > 0)
{!! Form::select('namapekerjaan', $daftarpekerjaan, null,['class' => 'form-control js-example-basic-single', 'id'=>'namapekerjaan','placeholder'=>'Pilih Pekerjaan']) !!}
@else
<p>Tidak ada pilihan Pekerjaan,silahkan buat dulu.</p>
@endif
@if ($errors->has('namapekerjaan'))
<span class="help-block">{{ $errors->first('namapekerjaan') }}</span>
@endif
</div>

{{-- Nama Penghasilan --}}
<div class="form-group">
{!! Form::label('namapenghasilan','Penhasilan',['class' => 'control-label']) !!}
@if(count($daftarpenghasilan) > 0)
{!! Form::select('namapenghasilan', $daftarpenghasilan, null,['class' => 'form-control js-example-basic-single', 'id'=>'namapenghasilan','placeholder'=>'Pilih Penghasilan']) !!}
@else
<p>Tidak ada pilihan Penghasilan,silahkan buat dulu.</p>
@endif
@if ($errors->has('namapenghasilan'))
<span class="help-block">{{ $errors->first('namapenghasilan') }}</span>
@endif
</div>

<div class="col-md-12">
	{{-- Foto --}}
	@if($errors->any())
	<div class="form-group {{ $errors->has('foto') ? 'has-error' : 'has-success' }}"></div>
	@else
	<div class="form-group">
	@endif
		{!! Form::label('foto','Foto') !!}
		{!! Form::file('foto') !!}
		@if ($errors->has('foto'))
		<span class="help-block">{{ $errors->first('foto') }}</span>
		@endif
	</div>
  </div>

{{-- Password 
@if($errors->any())
<div class="form-group {{ $errors->has('password') ? 'has-error' : 'has-success' }}"></div>
@else
<div class="form-group">
@endif
	{!! Form::label('password','Password',['class' => 'control-label']) !!}
	{!! Form::password('password', ['class' => 'form-control']) !!}
	@if ($errors->has('password'))
	<span class="help-block">{{ $errors->first('password') }}</span>
	@endif
</div>--}}

{{-- Submit button --}}
<div class="form-group">
	{!! Form::submit($submitButtonText,['class' => 'btn btn-primary form-control']) !!}
</div>

<script>
$(document).ready(function(){
	$('.js-example-basic-single').select2();
});
</script>