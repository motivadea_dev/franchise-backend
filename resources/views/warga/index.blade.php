@extends('template')

@section('main')
<div id="warga" class="panel panel-default">
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog" style="margin-top:100px;">
<div class="modal-dialog">
	<!-- Modal Produk-->
	<div class="modal-content">
		<div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <div><b>Impor Excel</b></div>
        </div>
        <div class="modal-body">
        	<form style="border: 2px solid #a1a1a1;margin-top: 15px;padding: 10px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">
				<input type="file" name="import_file" /><br>
				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<button class="btn btn-success">Import File Excel</button>
			</form>
		</div>
	</div>
</div>
</div>
<!-- End Modal -->
	<div class="panel-heading"><b><h4>Data Warga</h4></b></div>
	<div class="panel-body">
	@include('_partial.flash_message')
	@include('warga.form_pencarian')
	<div class="tombol-nav">
		{{ link_to('warga/create','Tambah Warga',['class' => 'btn btn-primary']) }}
		<a href="#" id="cmd_import"><button class="btn btn-success">Import Excel</button></a>
	</div><br><br><br>
	@if (count($daftarwarga) > 0)
	<table class="table">
		<thead>
			<tr>
			<!-- Tampil fild -->
				<th>No KTP</th>
				<th>Nama Warga</th>
				<th>Alamat</th>
				<th>Tanggal Lahir</th>
				<th>Jenis Kelamin</th>
				<th>action</th>
			</tr>
		</thead>
		<tbody>
			<?php $i=0; ?>
			<?php foreach($daftarwarga as $warga): ?>
			<tr>
				<!-- Tampil Data (Perulangan) -->
				<td>{{ $warga->noktp }}</td>
				<td>{{ $warga->nama }}</td>
				<td>{{ $warga->alamat }}</td>
				<td>{{ $warga->tanggal_lahir }}</td>
				@if($warga->jenis_kelamin == 'L')
				<td>Laki-Laki</td>
				@elseif($warga->jenis_kelamin == 'P')
				<td>Perempuan</td>
				@endif
				<!-- Tombol -->
				<td>
					<div class="box-button"> 
					{{ link_to('warga/' . $warga->id,'Detail',['class' => 'btn btn-success btn-sm']) }}</div>
					<div class="box-button">
					{{ link_to('warga/' . $warga->id . '/edit', 'Edit', ['class' => 'btn btn-warning btn-sm']) }}
					</div>
					<div class="box-button">
					{!! Form::open(['method' => 'DELETE', 'action' => ['WargawebController@destroy',$warga->id]]) !!}
					{!! Form::submit('Delete', ['class' => 'btn btn-danger btn-sm'])!!}
					{!! Form::close()!!}
					</div>
					<div class="box-button"> 
					{{ link_to('warga/print/' . $warga->id,'Cetak',['class' => 'btn btn-primary btn-sm','target'=>'_blank']) }}</div>
				</td>
			</tr>
		<?php endforeach ?>
		</tbody>
	</table>
	@else
	<p>Tidak ada data warga.</p>
	@endif
	<div class="table-nav">
	<div class="jumlah-data">
		<strong>Jumlah Warga : {{ $jumlahwarga }}</strong>
	</div>
	<div class="paging">
	{{ $daftarwarga->links() }}
	</div>
	</div>

	</div>
</div>
<script>
$(document).ready(function(){
	$("#cmd_import").click(function(){
		$("#myModal").modal({backdrop: "static"});
	});
});
</script>
@stop

@section('footer')
	@include('footer')
@stop