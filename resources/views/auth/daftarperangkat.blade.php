@extends('layouts.app')

@section('content')
<section id="wrapper">
        <div class="login-register" style="background-image:url({{ asset('adm/assets/images/background/login-register.jpg')}});">        
            <div class="login-box card">
            <div class="card-body">
                @include('_partial.flash_message')
                <form class="form-horizontal form-material" id="loginform" role="form" method="POST" enctype="multipart/form-data" action="{{ url('registrasidesa') }}">
                    {{ csrf_field() }}
                    <h3 class="box-title m-b-20" align="center">PENDAFTARAN PERANGKAT DESA</h3>
                    {!! Form::hidden('level','desa') !!}
                    {!! Form::hidden('status', 1) !!}
                    {!! Form::hidden('id_dusun', 1) !!}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="Nama" id="name" name="name" value="{{ old('name') }}">
                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('nohp') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="No Handphone" id="nohp" name="nohp" value="{{ old('nohp') }}">
                            @if ($errors->has('nohp'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('nohp') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="Email" id="email" name="email" value="{{ old('email') }}">
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" placeholder="Username" id="username" name="username" value="{{ old('username') }}">
                            @if ($errors->has('username'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('username') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" placeholder="Password" id="password" name="password" value="{{ old('password') }}">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" placeholder="Konfirmasi Password" id="password_confirmation" name="password_confirmation" value="{{ old('password_confirmation') }}">
                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <h4 class="box-title m-b-20" align="center">VERIFIKASI IDENTITAS PERANGKAT</h4>
                    <div class="form-group{{ $errors->has('fotoktp') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                        {!! Form::label('fotoktp','A. Foto KTP') !!}
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pastikan Foto KTP yang Di Upload:<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.Terlihat Jelas (Fokus pada KTP,tidak buram)<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.Foto Asli (Bukan fotocopy, hasil scan <br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;dan tidak editan)<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3.Jangan Memasukan Foto Selain KTP</p>
                        {!! Form::file('fotoktp') !!}
                        @if ($errors->has('fotoktp'))
                        <span class="help-block">{{ $errors->first('fotoktp') }}</span>
                        @endif
                        </div>
                    </div>
                    <hr>
                    <div class="form-group{{ $errors->has('fotowajah') ? ' has-error' : '' }}">
                        <div class="col-xs-12">
                        {!! Form::label('fotowajah','B. Foto Selfi Dengan KTP') !!}
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pastikan Foto KTP yang Di Upload:<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1.Letakan KTP Anda dekat dengan wajah<br>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2.Pastikan wajah dan foto KTP anda jelas</p>
                        {!! Form::file('fotowajah') !!}
                        @if ($errors->has('fotowajah'))
                        <span class="help-block">{{ $errors->first('fotowajah') }}</span>
                        @endif
                        </div>
                    </div>
                    <hr>
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="checkbox checkbox-success p-t-0 p-l-10">
                                <input id="checkbox-signup" type="checkbox">
                                <label for="checkbox-signup"> Saya setuju syarat & ketentuan</label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-success btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Daftar</button>
                        </div>
                    </div>
                    <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            <p>Sudah punya akun ? <a href="{{ url('/login') }}" class="text-success m-l-5"><b>Masuk</b></a></p>
                            <p>atau</p>
                            <p>Kembali ke <a href="{{ url('/home') }}" class="text-success m-l-5"><b>Halaman Utama</b></a></p>
                        </div>
                    </div>
                </form>
                
            </div>
          </div>
        </div>
        
    </section>
@endsection