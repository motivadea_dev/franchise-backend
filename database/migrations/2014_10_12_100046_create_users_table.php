<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('noktp')->unique();
            $table->string('nama');
            $table->text('alamat');
            $table->string('kota');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->enum('level', ['admin','mitra','produksi','penjualan','kurir']);
            $table->enum('status', ['1','2']);
            $table->string('fotoktp')->nullable;
            $table->string('fotowajah')->nullable;
            $table->string('nohp');
            $table->string('fotoprofile')->nullable;
            $table->timestamps();
        });

        Schema::table('keranjang', function(Blueprint $table) {
            $table->foreign('id_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('transaksipenjualan', function(Blueprint $table) {
            $table->foreign('id_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
        Schema::table('perangkat', function(Blueprint $table) {
            $table->foreign('id_users')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('keranjang', function(Blueprint $table) {
            $table->dropForeign('keranjang_id_users_foreign');
        });
        Schema::table('transaksipenjualan', function(Blueprint $table) {
            $table->dropForeign('transaksipenjualan_id_users_foreign');
        });
        Schema::table('perangkat', function(Blueprint $table) {
            $table->dropForeign('perangkat_id_users_foreign');
        });
        Schema::drop('users');
    }
}
