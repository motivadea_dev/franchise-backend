<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTransaksipenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaksipenjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kodepenjualan', 100)->unique();
            $table->integer('id_users')->unsigned();
            $table->date('tanggal');
            $table->double('totaldiskon');
            $table->double('totalbelanja');
            $table->double('subtotal');
            $table->enum('status',['order','proses','kirim','selesai']);
            $table->string('bukti')->nullable;
            $table->timestamps();
        });
        Schema::table('detailpenjualan', function(Blueprint $table) {
            $table->foreign('id_transaksipenjualan')
                ->references('id')
                ->on('transaksipenjualan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('detailpenjualan', function(Blueprint $table) {
            $table->dropForeign('detailpenjualan_id_transaksipenjualan_foreign');
        });
        Schema::drop('transaksipenjualan');
    }
}
