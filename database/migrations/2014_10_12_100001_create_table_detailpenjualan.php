<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableDetailPenjualan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detailpenjualan', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_transaksipenjualan')->unsigned();
            $table->integer('id_produk')->unsigned();
            $table->integer('jumlah');
            $table->timestamps();

            //Set Primary Key
            /*$table->primary(['id_transaksipenjualan', 'id_produktoko']);

            //Set Foreign Key ke Transaksi
            $table->foreign('id_transaksipenjualan')
                ->references('id')
                ->on('transaksipenjualan')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            //Set Foreign Key ke Produk
            $table->foreign('id_produktoko')
                ->references('id')
                ->on('produktoko')
                ->onDelete('cascade')
                ->onUpdate('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('detailpenjualan');
    }
}
