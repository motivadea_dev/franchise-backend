-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 24, 2018 at 05:31 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `onlineshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `detailpenjualan`
--

CREATE TABLE IF NOT EXISTS `detailpenjualan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_transaksipenjualan` int(10) unsigned NOT NULL,
  `id_produk` int(10) unsigned NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `detailpenjualan_id_transaksipenjualan_foreign` (`id_transaksipenjualan`),
  KEY `detailpenjualan_id_produk_foreign` (`id_produk`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kategoriproduk`
--

CREATE TABLE IF NOT EXISTS `kategoriproduk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `namakategori` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `kategoriproduk`
--

INSERT INTO `kategoriproduk` (`id`, `namakategori`, `foto`, `created_at`, `updated_at`) VALUES
(1, 'Gulali', '', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `keranjang`
--

CREATE TABLE IF NOT EXISTS `keranjang` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_users` int(10) unsigned NOT NULL,
  `id_produk` int(10) unsigned NOT NULL,
  `jumlah` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `keranjang_id_produk_foreign` (`id_produk`),
  KEY `keranjang_id_users_foreign` (`id_users`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_100001_create_table_detailpenjualan', 1),
('2014_10_12_100002_create_table_transaksipenjualan', 1),
('2014_10_12_100040_create_table_keranjang', 1),
('2014_10_12_100041_create_table_produk', 1),
('2014_10_12_100042_create_table_kategoriproduk', 1),
('2014_10_12_100043_create_table_perangkat', 1),
('2014_10_12_100046_create_users_table', 1),
('2014_10_12_100047_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `perangkat`
--

CREATE TABLE IF NOT EXISTS `perangkat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_users` int(10) unsigned NOT NULL,
  `app_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `perangkat_id_users_foreign` (`id_users`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `produk`
--

CREATE TABLE IF NOT EXISTS `produk` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_kategoriproduk` int(10) unsigned NOT NULL,
  `kodeproduk` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `namaproduk` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `stok` int(11) NOT NULL,
  `harga` double NOT NULL,
  `diskon` double NOT NULL,
  `foto` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `produk_kodeproduk_unique` (`kodeproduk`),
  KEY `produk_id_kategoriproduk_foreign` (`id_kategoriproduk`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `produk`
--

INSERT INTO `produk` (`id`, `id_kategoriproduk`, `kodeproduk`, `namaproduk`, `stok`, `harga`, `diskon`, `foto`, `created_at`, `updated_at`) VALUES
(1, 1, '12345', 'Sample Produk 1', 10, 5000, 0, 'sample1.jpg', NULL, NULL),
(2, 1, '12346', 'Sample Produk 2', 10, 5000, 0, 'sample2.jpg', NULL, NULL),
(5, 1, '123457', 'Sample Produk 3', 10, 5000, 0, 'sample3.jpg', NULL, NULL),
(6, 1, '123458', 'Sample Produk 4', 10, 5000, 0, 'sample4.jpg', NULL, NULL),
(7, 1, '123459', 'Sample Produk 5', 10, 5000, 0, 'sample5.jpg', NULL, NULL),
(8, 1, '123450', 'Sample Produk 6', 10, 5000, 0, 'sample6.jpg', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transaksipenjualan`
--

CREATE TABLE IF NOT EXISTS `transaksipenjualan` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kodepenjualan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_users` int(10) unsigned NOT NULL,
  `tanggal` date NOT NULL,
  `totaldiskon` double NOT NULL,
  `totalbelanja` double NOT NULL,
  `subtotal` double NOT NULL,
  `status` enum('order','proses','kirim','selesai') COLLATE utf8_unicode_ci NOT NULL,
  `bukti` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaksipenjualan_kodepenjualan_unique` (`kodepenjualan`),
  KEY `transaksipenjualan_id_users_foreign` (`id_users`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8_unicode_ci NOT NULL,
  `kota` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `level` enum('admin','mitra','produksi','penjualan','kurir') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('1','2') COLLATE utf8_unicode_ci NOT NULL,
  `fotoktp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fotowajah` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nohp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fotoprofile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `alamat`, `kota`, `email`, `password`, `remember_token`, `level`, `status`, `fotoktp`, `fotowajah`, `nohp`, `fotoprofile`, `created_at`, `updated_at`) VALUES
(1, 'Admin GJO', 'Jalan-jalan', 'semarang', 'admin@gjo.com', '$2y$10$qYX1WgLPv9KEN2JS0wCCS.1h25Wu9D6h81S/5X/PB6rxHoc5goXny', NULL, 'admin', '2', '', '', '08123456789', '', NULL, NULL),
(2, 'GJO Tlogosari', 'Jalan-jalan', 'semarang', 'gjotlogosari@gmail.com', '$2y$10$qYX1WgLPv9KEN2JS0wCCS.1h25Wu9D6h81S/5X/PB6rxHoc5goXny', NULL, 'mitra', '2', '', '', '081234723847', '', NULL, NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `detailpenjualan`
--
ALTER TABLE `detailpenjualan`
  ADD CONSTRAINT `detailpenjualan_id_produk_foreign` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `detailpenjualan_id_transaksipenjualan_foreign` FOREIGN KEY (`id_transaksipenjualan`) REFERENCES `transaksipenjualan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `keranjang`
--
ALTER TABLE `keranjang`
  ADD CONSTRAINT `keranjang_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `keranjang_id_produk_foreign` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `perangkat`
--
ALTER TABLE `perangkat`
  ADD CONSTRAINT `perangkat_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_id_kategoriproduk_foreign` FOREIGN KEY (`id_kategoriproduk`) REFERENCES `kategoriproduk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `transaksipenjualan`
--
ALTER TABLE `transaksipenjualan`
  ADD CONSTRAINT `transaksipenjualan_id_users_foreign` FOREIGN KEY (`id_users`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
